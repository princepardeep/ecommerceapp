
import { StackNavigator } from 'react-navigation'
// Sreens
import Home from './View/Home'


const DashboardConfiguration = {
  Home: {
    screen: Home,
    navigationOptions: {
      header: null
    }
  },
}
export default StackNavigator(DashboardConfiguration)


