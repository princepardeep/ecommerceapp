import React, { Component } from 'react'
import { View, Text, Image, ScrollView, FlatList, } from 'react-native'
import { baseStyles, colors, dimensions } from "../../styles/base";
import couponstyle from "../../styles/couponstyle";

import Header from "../../Common/Header";


const search = require('../../assets/search.png');
const Favorites = require('../../assets/ic_Favorites.png');
const catagoryView = require('../../assets/ic_tileViewInactive_.png');
const ic_tileViewActiveBlue = require('../../assets/ic_tileViewActiveBlue.png');
const ic_listViewInactive = require('../../assets/ic_listViewInactive.png');
const ic_couponBlue = require('../../assets/ic_couponBlue.png');
const drop_icon = require('../../assets/drop-down-arrow.png');


class Coupen extends Component {
  constructor(props) {
    super(props)


    this.state = {

      productList: [
        { price: "8 lbs.", weight: 10, imgLink: 'https://www.cyclegear.com/images/sites/cycle_gear/support/schema/logo-22bfee512d70bd00cb7ac4ed2a0e4b2f.png?vsn=d', currency: '' },
        { price: "10 lbs.", weight: 30, imgLink: 'https://knoji.com/images/logo/allposterscom-wide.jpg', currency: '' },
        { price: "80 lbs.", weight: 112, imgLink: 'https://sparkpix2-hireology.s3.amazonaws.com/richardfisherdealerships-theautobarn/autobarnlogo.png', currency: '' },
        { price: "500 lbs.", weight: 11, imgLink: 'https://pictures.attention-ngn.com/portal/24/216315/logo/1450196091.275_114_o.jpg', currency: 'booking' },
        { price: "80-10 lbs", weight: 43, imgLink: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH1F05sovkQijXQr4ydJAdC7nvy51K5HKLZNJdzvgmeQrDRTBZ', currency: '2' },
        { price: "80 lbs", weight: 200, imgLink: 'https://yt3.ggpht.com/a/AGF-l7-jpfLxSNFf7sm5MSTnrQGUN4QbAXivtTpWQg=s900-c-k-c0xffffffff-no-rj-mo', currency: '10' },

      ],

    }

  }




  render() {

    return (

      <View style={[baseStyles.container, { backgroundColor: '#dcdcdc' }]}>

        <Header title="SHOP" />



        <View style={couponstyle.seacrhContainer}>

          <Image source={search} style={{ height: 15, width: 15, alignSelf: 'center' }} />
          <Text style={{ color: '#dcdcdc', alignSelf: 'center', left: 5 }}>
            Search
        </Text>

        </View>

        <View style={{ flex: 1, backgroundColor: colors.white, marginTop: 8 }}>


          <View style={{ width: dimensions.width, height: 50, marginTop: 10, flexDirection: 'row' }}>

            <View style={couponstyle.uppersection}>

              <Text >
                Pounds captured
              </Text>
              <Image source={drop_icon} style={{ height: 8, width: 8 }} />



            </View>

            <View style={{ flex: 0.4, flexDirection: 'row', justifyContent: 'space-evenly' }}>

              <Image source={catagoryView} style={{ alignSelf: 'center' }} />
              <Image source={ic_tileViewActiveBlue} style={{ alignSelf: 'center' }} />
              <Image source={ic_listViewInactive} style={{ alignSelf: 'center' }} />


            </View>

          </View>



          <FlatList
            extraData={this.state}
            data={this.state.productList}
            showsVerticalScrollIndicator={false}

            renderItem={({ item }) =>

              <View style={couponstyle.flexStyle}>

                <View style={{
                  padding: 8,
                }}>

                  <Image source={Favorites} />

                  <Image source={{ uri: item.imgLink }} style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: 90,

                  }} resizeMode={'contain'} />

                  <Text style={{ alignSelf: 'center', fontSize: 10 }}>
                    {item.price} {''} / $ {item.currency}
                  </Text>

                  <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <Text style={couponstyle.textStyle}>
                      {item.weight} {''} LBS.
                  </Text>
                    <Image source={ic_couponBlue} style={{ bottom: 2, left: 25 }} />

                  </View>

                </View>

              </View>

            }

            numColumns={2}

            keyExtractor={(item, index) => index}

          />
        </View>



      </View >



    );
  }
}



export default (Coupen);
