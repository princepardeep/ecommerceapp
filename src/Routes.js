import React from 'react'
import {
  StackNavigator,
  TabNavigator, 
} from 'react-navigation';

import { Image, View, Text } from 'react-native'
import Splash from "./Splash";
import Coupen from "./Coupons/View/Coupen";
import Fav from "./Favourite/Fav";
import Featured from "./Featured/Featured";
import Catagory from "./Catagory/Catagory";
import More from "./More/More";

import { baseStyles, colors, dimensions } from "./styles/base";


const fav_icon = require('./assets/ic_navFavorites.png')
const feature_icon = require('./assets/ic_navFeatured.png')
const coupons_icon = require('./assets/ic_couponBlue.png')
const catagory_icon = require('./assets/ic_tileViewInactive_.png')
const more_icon = require('./assets/ic_navMore.png')


//StackNavigator
export const createRootNavigator = (user) => {
  return StackNavigator({
    Splash: {
      screen: Splash,
      navigationOptions: {
        header: null
      },
    },



    //Tab navigator
    tabscreen: {
      screen: TabNavigator(
        {
          Fav: {
            screen: Fav,
            navigationOptions: {
              header: null
            },
          },
          Featured: {
            screen: Featured,
            navigationOptions: {
              header: null
            },
          },
          Coupen: {
            screen: Coupen,
            navigationOptions: {
              header: null
            },
          },
          Catagory: {
            screen: Catagory,
            navigationOptions: {
              header: null
            },
          },
          More: {
            screen: More,
            navigationOptions: {
              header: null
            },
          },

        },
        {
          // initialRouteName: (user) ? "Dashboard" : "Auth",
          initialRouteName: "Coupen",

          // tabBarPosition: 'bottom',
          tabBarPosition: 'bottom',
          // animationEnabled: true,
          swipeEnabled: true,
          tabBarOptions: {
            showIcon: true,
            showLabel: false,
            activeBackgroundColor: colors.lightblack,
            inactiveBackgroundColor: colors.lightblack,
            style: {
              backgroundColor: colors.lightblack,
              height: 60,
            },
            indicatorStyle: {
              backgroundColor: 'transparent'
            }
          },

          navigationOptions: ({ navigation }) => ({

            tabBarIcon: ({ focused, tintColor }) => {

              const { routeName } = navigation.state;
              let iconName, screenName;

              if (routeName === 'Fav') {
                iconName = focused ? fav_icon : fav_icon;
                screenName = "Favorites"

              }



              else if (routeName === 'Featured') {
                iconName = focused ? feature_icon : feature_icon;
                screenName = "Featured"

              }
              else if (routeName === 'Coupen') {
                iconName = focused ? coupons_icon : coupons_icon;
                screenName = "Coupens"

              }
              else if (routeName === 'Catagory') {
                iconName = focused ? catagory_icon : catagory_icon;
                screenName = "Catagories"

              }
              else if (routeName === 'More') {
                iconName = focused ? more_icon : more_icon;
                screenName = "More"

              }

              return <View style={{ alignItems: 'center', height: 50, width: 60, justifyContent: 'center', top: 5 }}>

                <Image
                  style={{}} source={iconName} />
                <Text style={{ fontSize: 10, alignSelf: 'center', fontWeight: '400', color: screenName == "Coupens" ? colors.darkblue : colors.white, top: 5 }}>
                  {screenName}
                </Text>
              </View>


            }, showLabel: false, showIcon: true, header: null,
            tabBarOnPress: (tabOptions) => {
              if (tabOptions.scene.route.key == "Coupen") {


              }

              else if (tabOptions.scene.route.key == "Fav") {
                alert('coming soon.')


              }

              else if (tabOptions.scene.route.key == "Featured") {
                alert('coming soon.')
              }


              else if (tabOptions.scene.route.key == "Catagory") {
                alert('coming soon.')
              }

              else if (tabOptions.scene.route.key == "More") {
                alert('coming soon.')
              }

            }

          }),
          tabBarPosition: 'bottom',
          swipeEnabled: false,

        },

      ), navigationOptions: {
        header: null
      },
    }

  }, {
      initialRouteName: "Splash",
      // initialRouteName: "tabscreen",
    })
}
