import { StyleSheet, Dimensions } from 'react-native'

export const dimensions = {
  height: Dimensions.get('window').height,
  width: Dimensions.get('window').width
}

export const colors = {
  grey: '#777777',
  extralightgrey: '#CCCCCC',
  lightgrey: '#999999',
  darkgrey: '#555555',
  white: '#ffffff',
  black: "#000000",
  lightblack:'#323232',
   lightblue: "#618ebc",
  blue: "#0B1E45",
  orange: '#F65E26',
  darkblue: '#3a76b3',
  green: "#00D793",
  red: '#FF7E87'
}

export const padding = {
  vsm: 5,
  sm: 10,
  md: 15,
  lg: 20,
  xl: 30
}

export const margin = {
  sm: 10,
  md: 20,
  lg: 30,
  xl: 40
}

export const fonts = {

  sm: 12,
  md: 15,
  lg: 18,
  xl: 28,
  mdx: 18,

}


export const baseStyles = {

  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  titleTextContainer: {

    borderBottomWidth: 1,
    borderBottomColor: colors.tertiary,
    paddingBottom: padding.md
  },
  

}

export default function createStyles(overrides = {}) {
  return StyleSheet.create({ ...baseStyles, ...overrides })
}