import createStyles, { fonts, colors, padding, dimensions, baseStyles } from './base.js'

const couponStyles = {
    // flex: 0.3,
    seacrhContainer: { backgroundColor: colors.white, width: dimensions.width - 20, alignSelf: 'center', marginTop: 10, height: 35, borderRadius: 5, flexDirection: 'row', justifyContent: 'center' },
    uppersection: { flex: 0.6, borderColor: '#cccccc', borderWidth: 1, borderRadius: 5, height: 40, marginLeft: 10, alignSelf: 'center', alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10 },
    flexStyle: { flex: 1, flexDirection: 'column', margin: 10, borderColor: '#cccccc', borderWidth: 1, borderRadius: 5 },
    textStyle:{ alignSelf: 'center', fontSize: 12, color: '#0c993a', fontWeight: 'bold' }

}
export default styles = createStyles(couponStyles)